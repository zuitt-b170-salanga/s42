/* s42-ACTIVITY index.js */

// // const txtFirstName = document.querySelector('#txt-first-name');
// // const txtFullName = document.querySelector('#span-full-name');




// txtFirstName.addEventListener("keyup", (event) => {
// 	txtFullName.innerHTML = txtFirstName.value
// })




// /*
// 	event -- actions that the user is doing in our webpage (scroll,click,hover,keypress/type)

// 	addEventListener - a function that lets the webpage to listen to the events performed by
// 		the user 
// 			-- takes 2 arguments
// 				- string - the event on w/c the HTML element will listen
// 				- function - executed by the element once the event (1st argument) is triggered
// */




// txtFirstName.addEventListener("keyup", (event) => {
// 	// trying to log the codes, instead of its value
// 	console.log(event.target);
// 	// trying to log the value of the element, once the event is triggered
// 	console.log(event.target.value);
// })




// /*
// 	Make another "keyup" event where in the span element will record the Last Name in the 
// 	forms

// 	send the output in the batch google chat
// */


// // ==============================================
// // document.querySelector("#txt-last-name");

// // const txtLastName = document.querySelector('#txt-last-name');

// // txtFirstName.addEventListener("keyup", (event) => {
// // 	txtFullName.innerHTML = txtLastName.value
// // })


// // txtLastName.addEventListener("keyup", (event) => {
	
// // 	console.log(event.target);
	
// // 	console.log(event.target.value);
// // })

// // ==============================================

// // const txtLastName = document.querySelector('#txt-last-name');




// txtFirstName.addEventListener("keyup", (event) => {
// 	txtFullName.innerHTML = txtLastName.value;

// 	console.log(event.target);
	
// 	console.log(event.target.value);

// })




// // ==============================================

// // Copy the index.html and index.js to the activity folder.

// // Listen to an event when the last name's input is changed.
// // Instead of anonymous functions for each of the event listener:
// // Create a function that will update the span's contents based on the value of the first and last name input fields.
// // Instruct the event listeners to use the created function.
// // ===============================================




// // ===============================================

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
const txtLastName = document.querySelector('#txt-last-name');


const FullName = () => {
    let first, second;
    first = txtFirstName.value;
    second = txtLastName.value;
   

    spanFullName.innerHTML = `${first}${second}`;
}

	txtFirstName.addEventListener("keyup", FullName)
    txtLastName.addEventListener("keyup", FullName)
	


//=====================================================





